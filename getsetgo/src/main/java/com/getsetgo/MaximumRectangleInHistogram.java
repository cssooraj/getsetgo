package com.getsetgo;

import java.util.Stack;

public class MaximumRectangleInHistogram {

    public int largestRectangleArea(int[] heights) {
        Stack< Integer > stack = new Stack < > ();
        stack.push(-1);
        int maxarea = 0;
        for (int i = 0; i < heights.length; ++i) {
            while (stack.peek() != -1 && heights[stack.peek()] >= heights[i]) {
                maxarea = Math.max(maxarea, heights[stack.pop()] * (i - stack.peek() - 1));
                System.out.println("Area i" + i + "--->"+ maxarea);
            }
            stack.push(i);
            System.out.println("Height" + heights[i]);
            System.out.println("Stack" + heights[stack.peek()]);
        }
        while (stack.peek() != -1)
            maxarea = Math.max(maxarea, heights[stack.pop()] * (heights.length - stack.peek() -1));
        return maxarea;
    }

    public static void main(String args[]){
        MaximumRectangleInHistogram mh = new MaximumRectangleInHistogram();
        int input[] = {2,2,2,6,1,5,4,2,2,2,2};
        int maxArea = mh.largestRectangleArea(input);
        //System.out.println(maxArea);
        assert maxArea == 12;
    }

}
