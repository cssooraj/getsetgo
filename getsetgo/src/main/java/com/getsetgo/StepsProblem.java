package com.getsetgo;

public class StepsProblem {


    public int climbStairs(int n) {

        if (n==0 || n==1) {
            return n;
        }
        int[] memo = new int[n+1];
        Integer totalSteps = 0;

        totalSteps = calculateTotalSteps(n, totalSteps, 0, memo);
        return totalSteps;
    }

    private int calculateTotalSteps(int n, int totalSteps, int currentSum, int[] memo) {
        //System.out.println(""+ totalSteps + number+ currentSum);

        if (n==currentSum) {
            //totalSteps = totalSteps+1;
            return 1;
            //return totalSteps;
        }

        if (n<currentSum) {
            return 0;
        }

        if (memo[totalSteps]>0) {
            return memo[totalSteps];
        }


        memo[totalSteps] = calculateTotalSteps (n, totalSteps+1, currentSum+1, memo) +
                +calculateTotalSteps(n, totalSteps+2, currentSum+2, memo);
        return memo[totalSteps];

    }

}
