package com.getsetgo;

import java.util.Arrays;

public class PerfectSquares {
    public int numSquares(int n) {
        if (n == 0) {
            return 0;
        }
        int s =n/2;
        int[] squares = new int[s];
        int[] bucket = new int[n+1];
        Arrays.fill(bucket, n+1);
        bucket[0] = 0;
        for (int index=0; index< squares.length; index++) {
            squares[index] = index*index;
        }
        squares[0] =0;
        for (int index =0; index<n+1; index++) {
            for (int sIndex =0; sIndex<squares.length; sIndex++) {
                if (squares[sIndex]> index) {
                    break;
                } else {
                    bucket[index] = Math.min(bucket[index], bucket[index-squares[sIndex]]+1);
                }
            }
        }

        return bucket[n];
    }
}
