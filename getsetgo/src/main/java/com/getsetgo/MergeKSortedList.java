package com.getsetgo;

public class MergeKSortedList {


    /* Takes two lists sorted in increasing order, and merge
    their nodes together to make one big sorted list. Below
    function takes O(Log n) extra space for recursive calls,
    but it can be easily modified to work with same time and
    O(1) extra space  */
    public static Node2 SortedMerge(Node2 a, Node2 b) {
        Node2 result = null;
        /* Base cases */
        if (a == null)
            return b;
        else if (b == null)
            return a;

        /* Pick either a or b, and recur */
        if (a.data <= b.data) {
            result = a;
            result.next = SortedMerge(a.next, b);
        } else {
            result = b;
            result.next = SortedMerge(a, b.next);
        }

        return result;
    }

    // The main function that takes an array of lists
    // arr[0..last] and generates the sorted output
    public static Node2 mergeKLists(Node2 arr[], int last) {
        // repeat until only one list is left
        while (last != 0) {
            int i = 0, j = last;

            // (i, j) forms a pair
            while (i < j) {
                // merge List i with List j and store
                // merged list in List i
                arr[i] = SortedMerge(arr[i], arr[j]);

                // consider next pair
                i++;
                j--;

                // If all pairs are merged, update last
                if (i >= j)
                    last = j;
            }
        }

        return arr[0];
    }

    /* Function to print nodes in a given linked list */
    public static void printList(Node2 Node2) {
        while (Node2 != null) {
            System.out.print(Node2.data + " ");
            Node2 = Node2.next;
        }
    }

    public static void main(String args[]) {
        int k = 3; // Number of linked lists
        int n = 4; // Number of elements in each list

        // an array of pointers storing the head nodes
        // of the linked lists
        Node2 arr[] = new Node2[k];

        arr[0] = new Node2(1);
        arr[0].next = new Node2(3);
        arr[0].next.next = new Node2(5);
        arr[0].next.next.next = new Node2(7);

        arr[1] = new Node2(2);
        arr[1].next = new Node2(4);
        arr[1].next.next = new Node2(6);
        arr[1].next.next.next = new Node2(8);

        arr[2] = new Node2(0);
        arr[2].next = new Node2(9);
        arr[2].next.next = new Node2(10);
        arr[2].next.next.next = new Node2(11);

        // Merge all lists
        Node2 head = mergeKLists(arr, k - 1);
        printList(head);
    }
}
 class Node2 {
    int data;
    Node2 next;

    Node2(int data) {
        this.data = data;
    }
}


