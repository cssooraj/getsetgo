package com.getsetgo;


import org.joda.time.Interval;
import java.util.*;

public class BookingMeetingRoom {


        private Map<Interval, List<Interval>> graph;
        private Map<Integer, List<Interval> > nodesInComp;
        private Set<Interval> visited;

        public static void main(String[] strings) {
                int[][] intervals = {{1,2},{3,4}};
                BookingMeetingRoom bookingMeetingRoom = new BookingMeetingRoom();
                bookingMeetingRoom.minMeetingRooms(intervals);
        }


        public int minMeetingRooms(int[][] intervals) {

                if (intervals == null) {
                        return 0;
                }
                List<Meeting> meetings = new ArrayList<>();
                for (int i=0; i < intervals.length; i++) {
                       /* for (int j=0; j < intervals[0].length; j++) {
                                System.out.println("This is how it is" + intervals[i][j]);
                               // Meeting m = new Meeting (intervals[i][j])
                        }*/
                       Meeting m = new Meeting(intervals[i][0], intervals[i][1]);
                       meetings.add((Meeting) meetings);
                }
                Collections.sort(meetings, new Comparator<Meeting>() {
                        @Override
                        public int compare(Meeting o1, Meeting o2) {
                                return o1.startTime-o2.startTime;
                        }
                });
                int startMeeting =0;
                int endMeeting=0;
                for (Meeting meeting : meetings) {
                       // if ()
                }
                return 0;

        }

        public class Meeting{
                int startTime;
                int endTime;

                Meeting(int s, int e) {
                        startTime=s;
                        endTime=e;
                }
        }

        // return whether two intervals overlap (inclusive)
       /* private boolean overlap(Interval a, Interval b) {
            return a//.start <= b.end && b.start <= a.end;
        }

        // build a graph where an undirected edge between intervals u and v exists
        // iff u and v overlap.
        private void buildGraph(List<Interval> intervals) {
            graph = new HashMap<>();
            for (Interval interval : intervals) {
                graph.put(interval, new LinkedList<>());
            }

            for (Interval interval1 : intervals) {
                for (Interval interval2 : intervals) {
                    if (overlap(interval1, interval2)) {
                        graph.get(interval1).add(interval2);
                        graph.get(interval2).add(interval1);
                    }
                }
            }
        }

        // merges all of the nodes in this connected component into one interval.
        private Interval mergeNodes(List<Interval> nodes) {
            int minStart = nodes.get(0).start;
            for (Interval node : nodes) {
                minStart = Math.min(minStart, node.start);
            }

            int maxEnd = nodes.get(0).end;
            for (Interval node : nodes) {
                maxEnd= Math.max(maxEnd, node.end);
            }

            return new Interval(minStart, maxEnd);
        }

        // use depth-first search to mark all nodes in the same connected component
        // with the same integer.
        private void markComponentDFS(Interval start, int compNumber) {
            Stack<Interval> stack = new Stack<>();
            stack.add(start);

            while (!stack.isEmpty()) {
                Interval node = stack.pop();
                if (!visited.contains(node)) {
                    visited.add(node);

                    if (nodesInComp.get(compNumber) == null) {
                        nodesInComp.put(compNumber, new LinkedList<>());
                    }
                    nodesInComp.get(compNumber).add(node);

                    for (Interval child : graph.get(node)) {
                        stack.add(child);
                    }
                }
            }
        }

        // gets the connected components of the interval overlap graph.
        private void buildComponents(List<Interval> intervals) {
            nodesInComp = new HashMap();
            visited = new HashSet();
            int compNumber = 0;

            for (Interval interval : intervals) {
                if (!visited.contains(interval)) {
                    markComponentDFS(interval, compNumber);
                    compNumber++;
                }
            }
        }

        public List<Interval> merge(List<Interval> intervals) {
            buildGraph(intervals);
            buildComponents(intervals);

            // for each component, merge all intervals into one interval.
            List<Interval> merged = new LinkedList<>();
            for (int comp = 0; comp < nodesInComp.size(); comp++) {
                merged.add(mergeNodes(nodesInComp.get(comp)));
            }

            return merged;
        }
*/
}
