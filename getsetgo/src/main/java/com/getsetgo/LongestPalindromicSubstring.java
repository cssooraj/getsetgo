package com.getsetgo;

public class LongestPalindromicSubstring {
    public String longestPalindrome(String s) {
        if (s == null || s.length()==1 || s.length() ==0) {
            return s;
        }
        int[] indexes = {0,1};
        for (int index = 1; index < s.length(); index++) {
            int[] firstSet = getPal(s, index-1, index);
            int[] secondSet = getPal(s, index-1, index+1);

            if ((firstSet[1]-firstSet[0]) > (secondSet[1]-secondSet[0])) {
                indexes = setLongestOne(indexes, firstSet);

            } else {
                indexes = setLongestOne(indexes, secondSet);

            }
        }
        return s.substring(indexes[0], indexes[1]);
    }

    private int[] setLongestOne(int[] indexes, int[] indexToBeSet) {

        if (indexes[1]-indexes[0] > indexToBeSet[1]-indexToBeSet[0]) {
            return indexes;
        } else {
            return indexToBeSet;
        }
    }

    private int[] getPal(String s, int start, int end) {
        while (start >= 0 && end >=0 && start < s.length() && end < s.length()) {
            if (s.charAt(start)!=s.charAt(end)) {
                break;
            }
            start++;
            end--;
        }
        return new int[] {end+1, start};
    }
}
