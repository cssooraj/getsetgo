package com.getsetgo;

public class ReverseLinkedList {

    public static void main(String[] strings) {
        ReverseLinkedList rl = new ReverseLinkedList();
        int[] input = {2,3,5,5,6};
        ListNode listNode = new ListNode(1);
        ListNode traverse = listNode;
        for (int i: input) {
            ListNode temp = traverse;
            temp.next = new ListNode(i);
            traverse =  temp.next;
        }
        traverse.next = null;
        rl.reverseList(listNode);
    }

    public ListNode reverseList(ListNode head) {

        // Get the first element
        // Get the next element
        // set the nex of next element to first element
        // set the next of first element to null;

        // While iterating -use while loop
        // do the same as above

        // on recursion
        // = head;
        ListNode prevNode = null;
        ListNode nextNode = head;
        while (nextNode != null) {

            ListNode node = nextNode.next;


            nextNode.next = prevNode;
            prevNode = nextNode;
            nextNode=node;

            // head.next.next = head;
            //  head.next = null;
            //node =
        }
        return prevNode;
    }
}
