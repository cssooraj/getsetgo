package com.getsetgo;

import java.util.ArrayList;
import java.util.HashMap;

public class TImeBasedKeyValuePair {
    /** Initialize your data structure here. */
    HashMap<String, ArrayList<Holder>> bucket;
    public TImeBasedKeyValuePair() {
        bucket = new HashMap<String, ArrayList<Holder>>();
    }

    public void set(String key, String value, int timestamp) {
        if (bucket.containsKey(key)) {
            ArrayList<Holder> existingList= bucket.get(key);
            Holder holder = new Holder();
            holder.timeStamp = timestamp;
            holder.value = value;
            if (!existingList.contains(holder)) {
                existingList.add(holder);
                // bucket.put(key, existingList);
            }
        } else{
            ArrayList<Holder> existingList= new ArrayList<Holder>();
            Holder holder = new Holder();
            holder.timeStamp = timestamp;
            holder.value = value;

            existingList.add(holder);
            bucket.put(key, existingList);
        }
    }

    public String get(String key, int timestamp) {
        if (bucket.containsKey(key)) {
            ArrayList<Holder> existingList= bucket.get(key);
            for (Holder holder: existingList) {
                if (holder.timeStamp == timestamp) {
                    return holder.value;
                }
            }
        }
        return null;
    }

    class Holder{
        Integer timeStamp;
        String value;
    }
}
