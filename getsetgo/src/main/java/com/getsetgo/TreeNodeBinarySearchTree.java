package com.getsetgo;

public class TreeNodeBinarySearchTree {
    public TreeNode root;

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    //Iterative Function to insert a value in BST
    public boolean add(int value) {

        //If Tree is empty then insert Root with the given value inside Tree
        if (isEmpty()) {
            root = new TreeNode(value);
            return true;
        }

        //Starting from root
        TreeNode currentNode = root;

        //Traversing the tree untill valid position to insert the value
        while (currentNode != null) {

            TreeNode leftChild = currentNode.left;
            TreeNode rightChild = currentNode.right;

            //If the value to insert is less than root value then move to left subtree
            //else move to right subtree of root
            //and before moving check if the subtree is null, if it's then insert the value.
            if (currentNode.val > value) {
                if (leftChild == null) {
                    leftChild = new TreeNode(value);
                    currentNode.left = (leftChild);
                    return true;
                }
                currentNode = leftChild;
            } else {
                if (rightChild == null) {
                    rightChild = new TreeNode(value);
                    currentNode.right = (rightChild);
                    return true;
                }
                currentNode = rightChild;
            } //end of else
        } //end of while
        return false;
    }

    //Function to check if Tree is empty or not
    public boolean isEmpty()
    {
        return root == null; //if root is null then it means Tree is empty
    }

    //Just for Testing purpose
    public void printTree(TreeNode current)
    {
        if (current == null) return;

        System.out.print(current.val + ",");
        printTree(current.left);
        printTree(current.right);

    }
}
