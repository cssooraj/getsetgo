package com.getsetgo;

public class RemoveLinkedListItems {



     /*public class ListNode {

      }*/


        public ListNode removeElements(ListNode head, int val) {

            if (head == null) {
                return head;
            }

            ListNode outputNode = new ListNode(0);
            outputNode.next = head;
            ListNode traverseNode = head;
            ListNode previousNode = outputNode;
            while (traverseNode != null) {
                // ListNode temp = outputNode.next;
                if (traverseNode != null && traverseNode.val == val) {
                    //temp.next = temp.next.next;
                    previousNode.next = traverseNode.next;
                    //traverseNode = outputNode;
                } else {
                    previousNode = traverseNode;

                }
                traverseNode = traverseNode.next;
            }
            return outputNode.next;
        }

}
