package com.getsetgo;

public class RobbersProblem {
    public int rob(int[] num) {
        int prevMax = 0;
        int currMax = 0;
        for (int x : num) {
            int temp = currMax;
            currMax = Math.max(prevMax + x, currMax);
            prevMax = temp;
        }
        return currMax;
    }

    public static void main(String[] strings) {
        int[] nums = {2,1,1,2};
        RobbersProblem r = new RobbersProblem();
        r.rob(nums);
    }
}
