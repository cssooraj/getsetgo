package com.getsetgo;

public class ThePrisonProblemMain {

    public static void main(String[] arg) {
        //[0,1,0,1,1,0,0,1]
      //  7
        ThePrisonProblem thePrisonProblem = new ThePrisonProblem();
        int[] input = {0,1,0,1,1,0,0,1};
        thePrisonProblem.prisonAfterNDays(input, 200);
    }
}
