package com.getsetgo;

import java.util.*;

public class DataStreamAsDisjointIntervals {

   /** private Map<Integer, Integer> valueToBond;//map val to containing interval left bound
    private Map<Integer, Interval> bondToInterval; // store intervals in TreeMap <left bound, interval>

    public DataStreamAsDisjointIntervals() {
        valueToBond = new HashMap<>();
        bondToInterval = new TreeMap<>();
    }

    public void addNum(int val) {
        //contained in an existing interval
        if (valueToBond.containsKey(val)) {
            return;
        }
        //isolated number, no connection to its left or right
        if (!valueToBond.containsKey(val - 1) && !valueToBond.containsKey(val + 1)) {
            valueToBond.put(val, val);
            bondToInterval.put(val, new Interval(val, val));
            return;
        }
        //may connect to left, right or both
        int left = valueToBond.containsKey(val - 1) ? valueToBond.get(val - 1) : val;
        int right = valueToBond.containsKey(val + 1) ? bondToInterval.get(valueToBond.get(val + 1)).end : val;
        valueToBond.put(val, left);
        valueToBond.put(right, left);
        bondToInterval.remove(val + 1);
        bondToInterval.put(left, new Interval(left, right));
    }

    public List<Interval> getIntervals() {
        return new ArrayList<>(bondToInterval.values());
    }
    }**/

   public static void main(String[] strings) {
       DataStreamAsDisjointIntervals sr = new DataStreamAsDisjointIntervals();
    System.out.println("aasdas");
    // 1, 3, 7, 2, 6,
    sr.addNum(1);
    sr.addNum(3);
    sr.addNum(7);
    sr.addNum(2);
    sr.addNum(6);
   int[][] output = sr.getIntervals();

    }

   PriorityQueue<Integer> q;
    Set<Integer> vis;

    public DataStreamAsDisjointIntervals() {
        q = new PriorityQueue<Integer>();
        vis = new HashSet<Integer>();
    }

    public void addNum(int val) {
        if (!vis.contains(val)) {
            vis.add(val);
            q.add(val);
        }
    }

    public int[][] getIntervals() {
        List<int[]> ans = new ArrayList<int[]>();
        PriorityQueue<Integer> nq = new PriorityQueue<Integer>();
        int start = q.remove();
        int end = start;
        nq.add(start);

        while (!q.isEmpty()) {
            int i = q.remove();
            nq.add(i);
            if (i - end != 1) {
                ans.add(new int[] { start, end });
                start = i;
                end = i;
            } else {
                end = i;
            }
        }

        ans.add(new int[] { start, end });

        q = nq;

        int[][] a = new int[ans.size()][2];
        int idx = 0;
        for (int[] i : ans) {
            a[idx++] = i;
        }

        return a;
    }
}
