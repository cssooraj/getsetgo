package com.getsetgo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class WordBreakSolution {

    public static void main(String[] strings) {

              //  ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
        String s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab";
                ;
        List<String> wordDict = new ArrayList<String>();
        wordDict.add("aa");
        wordDict.add("aaaa");
        wordDict.add("aaaaa");
        wordDict.add("aaaaaaaa");
        wordDict.add("aaaaaaaaaaa");
        wordDict.add("aaaaaaaaaaaaaaa");
        wordDict.add("ox");

        WordBreakSolution wordBreakSolution = new WordBreakSolution();
        wordBreakSolution.memo= new Boolean[s.length()];
       // wordBreakSolution.wordBreak(s,wordDict);
        wordBreakSolution.wordBreak2(s,wordDict);

    }

    public boolean wordBreak(String s, List<String> wordDict) {
        return word_Break(s, new HashSet(wordDict), 0);
    }
    public boolean word_Break(String s, Set<String> wordDict, int start) {
        if (start == s.length()) {
            return true;
        }
        for (int end = start + 1; end <= s.length(); end++) {
            System.out.println("w1 end"+ end);
            if (wordDict.contains(s.substring(start, end)) && word_Break(s, wordDict, end)) {
                return true;
            }
        }
        return false;
    }

    public  Boolean[] memo = null;
    public boolean wordBreak2(String s, List<String> wordDict) {
        return word_Break2(s, new HashSet(wordDict), 0, memo);
    }
    public boolean word_Break2(String s, Set<String> wordDict, int start, Boolean[] memo) {
        if (start == s.length()) {
            return true;
        }
        if (memo[start] != null) {
            return memo[start];
        }
        for (int end = start + 1; end <= s.length(); end++) {
            System.out.println("w2 end"+ end);
            if (wordDict.contains(s.substring(start, end)) && word_Break2(s, wordDict, end, memo)) {
                return memo[start] = true;
            }
        }
        return memo[start] = false;
    }

}
