package com.getsetgo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BattleShipQuestionWithMain {

    public static void main(String[] args) {
        int N1 = 4;
        String S1 = "1B 2C,2D 4D", T1 = "2B 2D 3D 4D 4A";
        System.out.println(Arrays.toString(getResult(S1, T1)));
        int N2 = 3;
        String S2 = "1A 1B,2C 2C", T2 = "1B";
        System.out.println(Arrays.toString(getResult(S2, T2)));
        int N3 = 12;
        String S3 = "1A 2A,12A 12A", T3 = "12A";
        System.out.println(Arrays.toString(getResult(S3, T3)));
    }

    private static int[] getResult(String s, String t) {
        int[] res = new int[2];
        String[] ships = s.split(",");
        Set<String> visited = new HashSet<>();
        for(String ship : ships) {
            String[] xAndY = ship.split(" ");
            int topX = Integer.parseInt(xAndY[0].substring(0, xAndY[0].length() - 1));
            char topY = xAndY[0].charAt(xAndY[0].length() - 1);
            int bottomX = Integer.parseInt(xAndY[1].substring(0, xAndY[1].length() - 1));
            char bottomY = xAndY[1].charAt(xAndY[1].length() - 1);
            int cnt = 0;
            for(String cell : t.split(" ")) {
                if(visited.contains(cell))
                    continue;
                int x = Integer.parseInt(cell.substring(0, cell.length() - 1));
                char y = cell.charAt(cell.length() - 1);
                if(x >= topX && y >= topY && x <= bottomX && y <= bottomY) {
                    cnt++;
                    visited.add(cell);
                }
            }
            if(cnt == (bottomX - topX+1) * (bottomY - topY+1)) {
                res[0]++;
                continue;
            }
            if(cnt > 0)
                res[1]++;
        }
        return res;
    }
}
