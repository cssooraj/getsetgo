package com.getsetgo;

import java.util.Arrays;

public class TaskSchedular {
    public int leastInterval(char[] tasks, int n) {
        int[] map = new int[26];
        for (char c: tasks)
            map[c - 'A']++;
        Arrays.sort(map);
        int time = 0;
        while (map[25] > 0) {
            int i = 0;
            while (i <= n) {
                if (map[25] == 0)
                    break;
                if (i < 26 && map[25 - i] > 0)
                    map[25 - i]--;
                time++;
                i++;
            }
            Arrays.sort(map);
        }
        return time;
    }

    public static void main(String[] s) {
        // tasks = ["A","A","A","B","B","B"], n = 2
        TaskSchedular t = new TaskSchedular();
        char[] tasks = {'A','A','A','B','B','B'};
        t.leastInterval(tasks, 2);
    }
}
