package com.getsetgo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CloneGraphDFS {
    class Node {
        public int val;
        public List<Node> neighbors;

        public Node(int _val) {
            val = _val;
            neighbors = new ArrayList<Node>();
        }
    }
    private HashMap<Node, Node> visitedNode = new HashMap<Node, Node>();
    public Node cloneGraph(Node node) {
        if (node == null) {
            return node;
        }

        Node clonedNode = new Node(node.val);
        if (visitedNode.get(node)== null) {
            visitedNode.put(node, clonedNode);
        } else {
            return visitedNode.get(node);
        }

        for (Node traverse:node.neighbors) {
            clonedNode.neighbors.add(cloneGraph(traverse));
        }

        return clonedNode;
    }
}
