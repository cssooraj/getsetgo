package com.getsetgo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class MeetingRoomsII {

    public int minMeetingRooms(int[][] intervals) {
        PriorityQueue<Integer> meetingBucketList = new PriorityQueue<Integer>((o1, o2) -> {return o1 -o2;});
        if (intervals == null) {
            return 0;
        }
        if (intervals.length == 0 || intervals.length ==1) {
            return intervals.length;
        }

        List<Interval> intervalList = new ArrayList<Interval>();

        for (int i = 0; i < intervals.length; i++) {
            Interval interval = new Interval(intervals[i][0], intervals[i][1]);
            intervalList.add(interval);
        }
        Collections.sort(intervalList, (o1, o2) -> o1.start - o2.start);
        // meetingBucketList.add(intervalList.get(0).end);

        for (Interval interval: intervalList) {
            if (meetingBucketList.peek() != null && interval.start >= meetingBucketList.peek()) {
                meetingBucketList.poll();
            }
            meetingBucketList.add(interval.end);
        }



        return meetingBucketList.size();

    }

    private class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }
}
