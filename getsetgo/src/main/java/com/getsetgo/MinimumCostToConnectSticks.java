package com.getsetgo;

import java.util.PriorityQueue;

public class MinimumCostToConnectSticks {

    public int connectSticks(int[] sticks) {
        if (sticks == null || sticks.length == 0) return -1;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();

        for (int e : sticks)
            minHeap.offer(e);

        int cost = 0;
        while (minHeap.size() > 1) {
            int sum = minHeap.poll() + minHeap.poll();
            minHeap.offer(sum);
            cost += sum;
        }
        return cost;
    }
}
