package com.getsetgo;

public class SingleNumber {

    public static void main(String[] strings) {
        int[] input = {4,1,2,1,2};
        SingleNumber singleNumber = new SingleNumber();
        singleNumber.singleNumber(input);
    }

    public int singleNumber(int[] A) {
        int a = 0;
        for(int i = 0; i < A.length; i++){
            a ^= A[i];
        }
        return a;
    }

}
