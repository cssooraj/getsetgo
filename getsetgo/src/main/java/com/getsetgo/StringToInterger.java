package com.getsetgo;

public class StringToInterger {

    public int myAtoi(String str) {
        String strWithoutSpace = removeSpaceCharacter(str);
        if(strWithoutSpace==""){
            return 0;
        }
        char[] chrArray = strWithoutSpace.toCharArray();
        int value = 0;
        long temp = 0L;

        if(chrArray[0]=='.'){
            return 0;
        }
        for(int i=0;i<chrArray.length;i++){
            int ascValue = chrArray[i];
            if(ascValue >=48 && ascValue <=57){
                temp = temp*10+Character.getNumericValue(ascValue);
                value = value*10+Character.getNumericValue(ascValue);
                if(temp > 2147483647){
                    value=2147483647;
                }
                System.out.print("v: "+value);
            }else if(i>0 && value==0){

                return 0;
            }else if(i>0 && chrArray[i]=='.'){

                break;
            }
            else if( !(ascValue >=48 && ascValue <=57) && i==0 && chrArray[0]!='-'){
                return 0;
            }
        }
        if (chrArray[0]=='-' && temp > 2147483647){
            return 0-value-1;
        }
        if (chrArray[0]=='-' && value > 0){
            return 0-value;
        }
        return value;
    }

    public String removeSpaceCharacter(String str){
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)!=' '){
                return str.substring(i,str.length());
            }
        }
        return str;
    }






}
