package com.getsetgo;

public class TicTacToe {
    int[][] board;
    int countP1 = 0;
    int countP2 =0;

    public TicTacToe(int n) {
        this.board = new int[n][n];
    }

    public int move(int row, int col, int player) {
         if (board[row][col] == 1 || board[row][col] == 2) {
             return -1;
         }

        /* if (player == 1 && countP1 == board.length) {
              return checkIfThisWasAValidMove(row, col, player);
         }

        if (player == 2 && countP1 == board.length) {
            return checkIfThisWasAValidMove(row, col, player);
        }*/

         return 1;
    }

    private int checkIfThisWasAValidMove(int row, int col, int player) {

        if (row -1 >= 0) {
            if (board[row-1][col] == player) {
                return 1;
            }

            if (col-1>=0) {
                if (board[row-1][col-1] == player) {
                    return 1;
                }
            }
            if (col+1>=0) {
                if (board[row-1][col+1] == player) {
                    return 1;
                }
            }


        }

        if (row + 1 < board.length) {
            if (board[row+1][col] == player) {
                return 1;
            }

            if (col-1>=0) {
                if (board[row+1][col-1] == player) {
                    return 1;
                }
            }
            if (col+1>=0) {
                if (board[row+1][col+1] == player) {
                    return 1;
                }
            }


        }

        if (col-1>=0) {
            if (board[row][col-1] == player) {
                return 1;
            }
        }
        if (col+1>=0) {
            if (board[row][col+1] == player) {
                return 1;
            }
        }
        return -1;
    }

}
