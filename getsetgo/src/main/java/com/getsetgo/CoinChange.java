package com.getsetgo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class CoinChange {

    public int coinChange(int[] coins, int amount) {

        if (coins == null ) {
            return 0;

        }

        int[] tracker = new int[amount+1];

        Arrays.fill(tracker, amount+1);
        tracker[0] = 0;
        for (int trackerIndex=1; trackerIndex<=amount; trackerIndex++) {
            for (int cIndex = 0; cIndex<coins.length; cIndex++) {
                if (coins[cIndex]<=trackerIndex) {
                    tracker[trackerIndex] = Math.min(tracker[trackerIndex], tracker[trackerIndex-coins[cIndex]]+1);
                }
            }
        }
        return tracker[amount] > amount ? -1 : tracker[amount];
    }


    public int coinChangeR(int[] coins, int amount) {
        if (amount < 1) return 0;
        return coinChangeR(coins, amount, new int[amount]);
    }

    private int coinChangeR(int[] coins, int rem, int[] count) {
        if (rem < 0) return -1;
        if (rem == 0) return 0;
        if (count[rem - 1] != 0) return count[rem - 1];
        int min = Integer.MAX_VALUE;
        for (int coin : coins) {
            int res = coinChangeR(coins, rem - coin, count);
            if (res >= 0 && res < min)
                min = 1 + res;
        }
        count[rem - 1] = (min == Integer.MAX_VALUE) ? -1 : min;
        return count[rem - 1];
    }

    public int coinChangeInRecursion(int[] coin, int amount) {
        Arrays.sort(coin);
        Set<String> visitedNode = new HashSet<>();
        int totalCount = runRecursiveToFindMinCombination(coin,amount,0,coin.length-1,0,visitedNode);

        if (totalCount>amount) {
            return -1;
        }

        return totalCount;
    }

    private int  runRecursiveToFindMinCombination(int[] coin, int amount, int computedSum, int index,
                                                  int coinsUsed, Set<String> visitedNode) {

        if (visitedNode.contains(computedSum+"/"+index+"/"+coin)) {
           return amount+1;
        }
        visitedNode.add(computedSum+"/"+index+"/"+coin);

        if (computedSum > amount) {
            return amount+1;
        }

        if (computedSum == amount) {
            return coinsUsed;
        }

        if ((index<0)) {
            return amount+1;
        }
        // computedSum = computedSum+;
        int firstCombination = runRecursiveToFindMinCombination(coin,
                amount, computedSum+coin[index], index, coinsUsed+1, visitedNode);
        int secondCombination = runRecursiveToFindMinCombination(coin, amount,
                0, index-1, 0, visitedNode);
        int third =  runRecursiveToFindMinCombination(coin, amount,
                computedSum +coin[index], index-1, coinsUsed+1, visitedNode);

        int totalCount = Math.min (Math.min(firstCombination,secondCombination),third);

        /*if (totalCount == amount+1) {
            return -1;
        }*/
        return totalCount;
    }

}
