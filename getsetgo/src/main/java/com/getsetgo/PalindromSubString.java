package com.getsetgo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PalindromSubString {

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        getResult(result, new ArrayList<Integer>(), candidates, target, 0);

        return result;
    }

    private void getResult(List<List<Integer>> result, List<Integer> cur, int candidates[], int target, int start){
        if(target > 0){
            for(int i = start; i < candidates.length && target >= candidates[i]; i++){
                cur.add(candidates[i]);
                getResult(result, cur, candidates, target - candidates[i], i);
                cur.remove(cur.size() - 1);
            }//for
        }//if
        else if(target == 0 ){
            result.add(new ArrayList<Integer>(cur));
        }//else if
    }


        public List<List<Integer>> combinationSum2(int[] candidates, int target) {
            List<Integer> solutionSet = new ArrayList<Integer>();
            List<List<Integer>> combinationSet = new ArrayList<List<Integer>>();
            if (candidates == null) {
                return combinationSet;
            }

            getCombinations(solutionSet, 0, target, 0, candidates, combinationSet);
            return combinationSet;
        }

        public void getCombinations(List<Integer> solutionSet,
                                    int nextNumber, int target,
                                    int currentSum, int[] candidates,
                                    List<List<Integer>> combinationSet) {

           if (currentSum+candidates[nextNumber]> target) {
                //solutionSet.clear();
                return;
           }

            if (currentSum + candidates[nextNumber] == target) {
                //solutionSet.add(candidates[nextNumber]);
                if (!combinationSet.contains(solutionSet)) {
                    combinationSet.add(solutionSet);
                }
                return;
            } else {
               solutionSet.add(candidates[nextNumber]);
                //   System.out.println(candidates[nextNumber]);
                // System.out.println(candidates[nextNumber]+currentSum

//);
            }

            currentSum =candidates[nextNumber]+currentSum;

            for (int index = 0; index < candidates.length; index++) {
                //solutionSet.add(candidates[nextNumber]);
                //getCombinations(new ArrayList<Integer>(solutionSet),index,target,currentSum, candidates, combinationSet);
                getCombinations(new ArrayList<Integer>(),index,target,currentSum, candidates, combinationSet);
                //solutionSet.remove(solutionSet.size()-1);
            }


           // getCombinations(new ArrayList<Integer>(solutionSet),nextNumber,target,currentSum, candidates, combinationSet);
            //getCombinations(new ArrayList<Integer>(solutionSet), nextNumber+1, target, currentSum, candidates, combinationSet);
            // getCombinations(new ArrayList<Integer>(),nextNumber,target,0, candidates, combinationSet);
          //  getCombinations(new ArrayList<Integer>(), nextNumber+1, target, 0, candidates, combinationSet);

        }

}
