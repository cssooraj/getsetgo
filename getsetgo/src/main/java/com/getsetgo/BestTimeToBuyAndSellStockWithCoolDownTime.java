package com.getsetgo;

public class BestTimeToBuyAndSellStockWithCoolDownTime {

    public int maxProfit(int[] prices) {

        if (prices == null || prices.length ==0) {
            return 0;
        }
        int[] profitTracker = new int[prices.length];
        int totalProfit = 0;
        int currentMax = prices[0];
        int currentMin =prices[0];
        profitTracker[0] = 0;
        for (int index = 1; index < prices.length; index++) {
            if (prices[index] < prices[index-1]) {
                //  if (current<a)
                profitTracker[index] = 0;
                //System.out.println(totalProfit+ "totalProfit");
                currentMin = prices[index];
                currentMax = prices[index];
            } else {
                currentMin=Math.min(currentMin, prices[index]);
                currentMax = Math.max(currentMax, prices[index]);
                profitTracker[index] = currentMax - currentMin;
            }

        }

        //i//nt[] output = new int[prices.length];
        for (int index =0; index < profitTracker.length; index++) {
           if (index -3 >=0) {
               profitTracker[index] = Math.max(profitTracker[index-3] + profitTracker[index], profitTracker[index-1]);
           } else {
               if (index-1 >=0) {
                   profitTracker[index] = Math.max(profitTracker[index], profitTracker[index - 1]);
               }
           }
        }

        /*if (currentMax != currentMin && currentMax>currentMin) {
            totalProfit =  totalProfit + (currentMax- currentMin);
        }*/

        return profitTracker[prices.length-1];
    }
}
