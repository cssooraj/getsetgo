package com.getsetgo;

public class NumberOfDistinctIslandsMain {

    public static void main(String[] strings) {
        int[][] input = {
                {1,1,0,1,1},
                {1,0,0,0,0},
                {0,0,0,0,1},
                {1,1,0,1,1}
        };
        NumberOfDistinctIslandsPart1 p1 = new NumberOfDistinctIslandsPart1();
        System.out.println(p1.numDistinctIslands(input));

    }
}
