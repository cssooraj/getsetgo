package com.getsetgo;

public class RemoveLinkedListItemsMain {
    public static void main(String[] strings) {
        int[] input = {2,3,6,4,5,6};
       //int[] input = {1};
        ListNode listNode = new ListNode(1);
        ListNode temp = listNode;

        for (int ip : input) {
            ListNode newNode = new ListNode(ip);
            temp.next = newNode;
            temp = temp.next;
        }

        RemoveLinkedListItems rm = new RemoveLinkedListItems();
        rm.removeElements(new ListNode(1), 1);

    }
}
