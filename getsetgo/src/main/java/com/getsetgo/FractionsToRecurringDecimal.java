package com.getsetgo;

import java.util.HashMap;
import java.util.Map;

public class FractionsToRecurringDecimal {
    public String fractionToDecimal(int numerator, int denominator) {
        if (denominator == 0) {
            return "";
        }

        if (numerator == 0) {
            return "0";
        }

        StringBuilder output = new StringBuilder();
        Long lNum = Long.valueOf(numerator);
        Long lDeno = Long.valueOf(denominator);
        Long quotient = lNum/lDeno;
        output.append(String.valueOf(quotient));
        Long remainder = lNum%lDeno;
        if (remainder == 0) {
            return output.toString();
        }
        Map<Long, Integer> remainderCheck = new HashMap<Long, Integer>();
        output.append(".");
        StringBuilder rb = new StringBuilder();
        while (remainder != 0) {

            if (remainderCheck.containsKey(remainder)) {
               // output.append("("+ String.valueOf(remainder*10/lDeno)+ ")");
                output.insert(remainderCheck.get(remainder), "(");
                output.append(")");
                return output.toString();
            }
            //Long quotient = remainder/lDeno;
            remainderCheck.put(remainder,output.length());
            remainder *= 10;
            output.append(String.valueOf(remainder/lDeno));
            remainder %=lDeno;


        }
        return output.toString();


    }

    public static void main(String[] strings) {
        FractionsToRecurringDecimal fd = new FractionsToRecurringDecimal();
        fd.fractionToDecimal(2,3);
    }
}
