package com.getsetgo;

import java.util.HashMap;

public class LFUCache {
    int size;
    DLinkedNode head;
    DLinkedNode tail;
    HashMap<Integer, DLinkedNode> tracker;
    int numberOfElements;

    public LFUCache(int capacity) {
        this.size = capacity;
        head = new DLinkedNode();
        tail = new DLinkedNode();
        tracker = new HashMap<Integer, DLinkedNode>();
        head.right = tail;
        tail.left = head;
    }

    public int get(int key) {
        if (tracker.get(key) != null ) {
            DLinkedNode keyNode = tracker.get(key);
            removeNode(keyNode);
            addNode(keyNode);
            return keyNode.val;
        }
        return -1;
    }

    public void put(int key, int value) {

        DLinkedNode fromTracker = tracker.get(key);

        if (fromTracker == null) {
            DLinkedNode newItem = new DLinkedNode();
            newItem.key = key;
            newItem.val = value;
            /*DLinkedNode currentH = head.right;
            head.right = newItem;
            newItem.left = head;
            newItem.right = currentH;
            currentH.left = newItem;*/
            addNode(newItem);
            numberOfElements++;
            if (numberOfElements>size) {
                DLinkedNode toBeRemoved = tail.left;
                removeNode(tail.left);
                tracker.remove(toBeRemoved.key);
                numberOfElements--;
            }

        } else {
            fromTracker.val = value;
            moveToHead(fromTracker);
        }




    }

    class DLinkedNode {

        DLinkedNode right;
        DLinkedNode left;
        int val;
        int key;
        int size;
    }
    void addNode(DLinkedNode node) {
        node.left = head;
        node.right =head.right;
        head.right.left = node;
        head.right = node;
        this.tracker.put(node.key, node);

    }

    void moveToHead(DLinkedNode node) {

        //remove from current position
        // move to head
        removeNode(node);
        addNode(node);

    }

    void removeNode(DLinkedNode node) {
        DLinkedNode prevNode = node.left;
        DLinkedNode nextNode = node.right;

        if (prevNode != null) {
            prevNode.right = nextNode;
        }

        if (nextNode != null) {

            nextNode.left = prevNode;
        }
    }


}
