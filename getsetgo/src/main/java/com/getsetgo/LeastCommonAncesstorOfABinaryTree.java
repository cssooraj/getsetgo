package com.getsetgo;
;

import java.util.*;

public class LeastCommonAncesstorOfABinaryTree {

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        // Stack for tree traversal
        Stack<TreeNode> stack = new Stack<>();

        // HashMap for parent pointers
        Map<TreeNode, TreeNode> parent = new HashMap<>();

        parent.put(root, null);
        stack.push(root);

        // Iterate until we find both the nodes p and q
        while ((!parent.containsKey(p) || !parent.containsKey(q)) && !stack.isEmpty()) {

            TreeNode node = stack.pop();

            // While traversing the tree, keep saving the parent pointers.
            if (node.left != null) {
                parent.put(node.left, node);
                stack.push(node.left);
            }
            if (node.right != null) {
                parent.put(node.right, node);
                stack.push(node.right);
            }
        }

        // Ancestors set() for node p.
        Set<TreeNode> ancestors = new HashSet<>();

        // Process all ancestors for node p using parent pointers.
        while (p != null) {
            ancestors.add(p);
            p = parent.get(p);
        }

        // The first ancestor of q which appears in
        // p's ancestor set() is their lowest common ancestor.
        while (!ancestors.contains(q) && q !=null)
            q = parent.get(q);
        return q;
    }

    public static void main(String[] strings) {
        Integer[] input = {3,5,1,6,2,0,8,null,null,7,4};//, p = 5, q = 1
        TreeNodeBinarySearchTree testOutBinarySearchTree = new TreeNodeBinarySearchTree();
        for (Integer integer: input) {
            if (integer != null)
            testOutBinarySearchTree.add(integer);
            // treeNode.left = new TreeNode(integer);
        }

        //TreeNode treeNode = new TreeNode(3);
        /*TreeNode tempNode = treeNode;
        for (Integer integer: input) {
            tempNode.right= new TreeNode(integer);
            treeNode.right= tempNode.right;
            tempNode = treeNode.right;
           // treeNode.left = new TreeNode(integer);
        }
*/
        TreeNode p = new TreeNode(5);
        TreeNode q = new TreeNode(1);
        LeastCommonAncesstorOfABinaryTree lt = new LeastCommonAncesstorOfABinaryTree();
        lt.lowestCommonAncestor(testOutBinarySearchTree.getRoot(), p, q);

    }
}
