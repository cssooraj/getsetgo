package com.getsetgo;

public class SearchSuggestionSystemMain {
    public static void main(String[] strings) {

        SearchSuggestionSystem searchSuggestionSystem = new SearchSuggestionSystem();
        searchSuggestionSystem.
                suggestedProducts(new String[]{"mobile","mouse","moneypot","monitor","mousepad"}, "mouse");
    }
}
