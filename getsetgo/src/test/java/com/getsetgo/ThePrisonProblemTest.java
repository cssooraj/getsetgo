package com.getsetgo;

import org.junit.Test;

public class ThePrisonProblemTest {

    @Test
    public void justTestForABasicCase() {

       int[] input =  {1,0,0,1,0,0,1,0};

        ThePrisonProblem thePrisonProblem = new ThePrisonProblem();
        thePrisonProblem.prisonAfterNDays(input, 1000000000);
    }
}
