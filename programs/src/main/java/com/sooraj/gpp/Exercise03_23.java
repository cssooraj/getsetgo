package com.sooraj.gpp;

import java.util.Scanner;

public class Exercise03_23 {

    public static void main(String[] strings) {
        Scanner in = new Scanner(System.in);
        double xInput =0;
        double yInput =0;
        System.out.print("Enter x coordinate");
        xInput =  in.nextDouble();
        System.out.println("Enter y corrdinate ");
        yInput =  in.nextDouble();
        in.close();

        if ((Math.abs(xInput)<= 10/2) && (Math.abs(yInput) <= 5.0/2)) {
            System.out.println("Point ( " + String.valueOf(xInput) + ", " + String.valueOf(yInput) + ") is in the rectangle");
        } else {
            System.out.println("Point ( " + String.valueOf(xInput) + ", " + String.valueOf(yInput) + ") is not in the rectangle");
        }

    }
}
