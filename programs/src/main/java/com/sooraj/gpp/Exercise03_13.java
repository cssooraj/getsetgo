package com.sooraj.gpp;

import java.util.Scanner;

public class Exercise03_13 {

    int filingStatus = -1;
    double taxableIncome = -1;
    double totalTax = 0;
    final double TEN_PERCENT = 0.10;
    final double FIFTEEN_PERCENT =0.15;
    final double TWENTY_FIVE_PERCENT = 0.25;
    final double TWENTY_EIGHT_PERCENT =0.28;
    final double THIRTY_THREE_PERCENT = 0.33;
    final double THIRTY_FIVE_PERCENT = 0.35;
    final int SINGLE_STATUS = 0;
    final int MARRIED_JOIN_FILING_OR_DIVORCED = 1;
    final int MARRIED_FILED_SEPARATE = 2;
    final int HEAD_OF_HOUSEHOLD = 3;

    public void Exercise03_13() {
        this.filingStatus=-1;
        this.taxableIncome=-1;
        this.totalTax=0;
    }

    public static void main(String[] arg) {

        Exercise03_13 ex = new Exercise03_13();
        ex.getUserInputsForStatusAndTaxableIncome();
        ex.validateInputs();
        //ex.filingStatus =0;
        //ex.taxableIncome =100000;
        ex.computeTheNexTax();
        System.out.println("totalTax" + ex.totalTax);

    }

    private void validateInputs() {
        if (filingStatus < 0 || taxableIncome < 0) {
            System.out.println("User input is invalid... System is exiting");
            System.exit(1);
        }

        if (taxableIncome == 0) {
            System.out.println("Taxable income is zero... System is exiting");
            System.exit(1);
        }
    }

    public int getUserInputsForStatusAndTaxableIncome() {
        //int filingStatus = -1;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the filling status: ");
        filingStatus =  in.nextInt();
        System.out.println("Enter the taxable income: ");
        taxableIncome =  in.nextDouble();
        in.close();
        return filingStatus;
    }

    /*public double recieveTaxableIncome() {
        //double taxableIncome = -1;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter your name: ");

        System.out.println("Name is: " + String.valueOf(taxableIncome));
        in.close();
        return taxableIncome;
    }*/

    public void computeTheNexTax() {
        switch (filingStatus) {
            case SINGLE_STATUS :
                computeTaxForFirstSlab();
                break;
            case MARRIED_JOIN_FILING_OR_DIVORCED :
                computeTaxForMarriedJointOrWindowed();
                break;
            case MARRIED_FILED_SEPARATE :
                computeTaxForMarriedFiledSeparate();
                break;
            case HEAD_OF_HOUSEHOLD :
                computeTaxForHeadOfHouseHold();
                break;
            default : System.out.println("Encountered error: The input status is probably invalid");
        }
    }

    private void computeTaxForHeadOfHouseHold() {
        computeForLowerBound(11950);
        computeForUpperAndLowerBounds(11950,45500,FIFTEEN_PERCENT);
        computeForUpperAndLowerBounds(45500,117450,TWENTY_FIVE_PERCENT);
        computeForUpperAndLowerBounds(117450,190200,TWENTY_EIGHT_PERCENT);
        computeForUpperAndLowerBounds(190200,372950,THIRTY_THREE_PERCENT);
        computeForUpperBoundAlone(372950, THIRTY_FIVE_PERCENT);
    }

    private void computeTaxForMarriedFiledSeparate() {
        computeForLowerBound(8350);
        computeForUpperAndLowerBounds(8350,33950,FIFTEEN_PERCENT);
        computeForUpperAndLowerBounds(33950,68525,TWENTY_FIVE_PERCENT);
        computeForUpperAndLowerBounds(68525,104425,TWENTY_EIGHT_PERCENT);
        computeForUpperAndLowerBounds(104425,186475,THIRTY_THREE_PERCENT);
        computeForUpperBoundAlone(186475, THIRTY_FIVE_PERCENT);
    }

    private void computeTaxForMarriedJointOrWindowed() {
        computeForLowerBound(16700);
        computeForUpperAndLowerBounds(16700,67900,FIFTEEN_PERCENT);
        computeForUpperAndLowerBounds(67900,137050,TWENTY_FIVE_PERCENT);
        computeForUpperAndLowerBounds(137050,208850,TWENTY_EIGHT_PERCENT);
        computeForUpperAndLowerBounds(208850,372950,THIRTY_THREE_PERCENT);
        computeForUpperBoundAlone(372950, THIRTY_FIVE_PERCENT);
    }

    private void computeTaxForFirstSlab() {

        /*if (lowerBound >= taxableIncome) {
            totalTax = taxableIncome * TEN_PERCENT;
            return true;
        } else {
            totalTax = 8350 * TEN_PERCENT;
        }*/
         computeForLowerBound(8350);

       /* if (taxableIncome > 8350) {
            if (33950 <= taxableIncome) {
                totalTax = totalTax + (33950-8350) * 0.15;
            } else {
                totalTax = totalTax + (taxableIncome -8359)*0.15;
            }

        }*/
        computeForUpperAndLowerBounds(8350,33950,FIFTEEN_PERCENT);
        computeForUpperAndLowerBounds(33950,82250,TWENTY_FIVE_PERCENT);
        computeForUpperAndLowerBounds(82250,171550,TWENTY_EIGHT_PERCENT);
        computeForUpperAndLowerBounds(171550,373950,THIRTY_THREE_PERCENT);
        computeForUpperBoundAlone(372950, THIRTY_FIVE_PERCENT);


    }

    private void computeForLowerBound(double lowerBound) {
        if (lowerBound >= taxableIncome) {
            totalTax = taxableIncome * TEN_PERCENT;

        } else {
            totalTax = lowerBound * TEN_PERCENT;
        }
    }

    private void computeForUpperAndLowerBounds(double lowerBound, double upperBound, double percent) {
        if (lowerBound < taxableIncome) {
            totalTax += (taxableIncome <= upperBound) ? (taxableIncome - lowerBound) * percent :
                    (upperBound - lowerBound) * percent;
        }
    }

    private void computeForUpperBoundAlone(double upperBound, double percent) {
        if (taxableIncome > upperBound) {
            totalTax += (taxableIncome - upperBound) * percent;
        }
    }



}


